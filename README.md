# StockpiApp



## Installation
Install Linux, install Gambas3
Use git pull and open in Gambas3 IDE.
Open project properties, libraries, add stockpilib (if not done yet)
If stockpilib not available yet:
Get the stockpilib from the same stockpi repo, and open it, make executable.

## Usage
Run from the Gambas3 IDE to check that everything works fine.
Then choose make executable, mark options as "desktop link".
From then on you can start it from the icon on the desktop.


## Support
Radio Show, Issue tracker, ..

## Roadmap
Must become the Stockpi project desktop app in Linux, has a command line alternative in StockpiCLI, and gets all needed funtions from StockpiLib.

## Contributing
Currently we werk together on this project in the radioshow webgang on Radio Centraal, any help welcome.

## Authors and acknowledgment
Wim.webgang, Marthe, Silvia

## License
GPL v3

## Project status
Active (starting up)
